//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////                                                                                                          ////
////  TTGO ESP8266 0.91 inch OLED demo w/ OTA                                                                 ////
////  requires U8g2 library                                                                                   ////
////  Select "NodeMCU 0.9 (ESP-12 Module)" board                                                              ////
////  More examples at http://simplestuffmatters.com                                                          ////
////  USB drivers: https://www.silabs.com/products/development-tools/software/usb-to-uart-bridge-vcp-drivers  ////
////                                                                                                          ////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <ESP8266WiFi.h>  //For ESP8266
#include <ESP8266WiFiMulti.h>
#include <ESP8266mDNS.h>  //For OTA
#include <WiFiUdp.h>      //For OTA
#include <ArduinoOTA.h>   //For OTA
#include <Arduino.h>
#include <U8g2lib.h>      // make sure to add U8g2 library and restart Arduino IDE  
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
#include <Adafruit_MCP9808.h>



#define OLED_SDA  2
#define OLED_SCL 14
#define OLED_RST  4

#define SEALEVELPRESSURE_HPA (1013.25)
Adafruit_BME280 bme; // I2C
TwoWire i2c;
ESP8266WiFiMulti wifiMulti;

Adafruit_MCP9808 mcp9808 = Adafruit_MCP9808();

U8G2_SSD1306_128X32_UNIVISION_F_SW_I2C u8g2(U8G2_R0, OLED_SCL, OLED_SDA , OLED_RST);
const char *text = "Milog temp sensor";  // scroll this text from right to left

//WIFI configuration
#include "secrets.h"
/* In secrets.h define something like this below.
 * #define wifi_ssid "myssid"
 * #define wifi_password "mysecretpassword"
 */

#define WiFi_hostname "ESP8266-TTGO"

//Necesary to make Arduino Software autodetect OTA device
WiFiServer TelnetServer(8266);

void setup_wifi() {
  delay(100);
  Serial.println("");
  Serial.print("Connecting to ");
  WiFi.hostname(WiFi_hostname);
  // see https://github.com/esp8266/Arduino/blob/master/libraries/ESP8266WiFi/examples/WiFiMulti/WiFiMulti.ino
  #ifdef wifi_ssid1
  wifiMulti.addAP(wifi_ssid1, wifi_password1);
  #endif
  #ifdef wifi_ssid2
  wifiMulti.addAP(wifi_ssid2, wifi_password2);
  #endif
  #ifdef wifi_ssid3
  wifiMulti.addAP(wifi_ssid3, wifi_password3);
  #endif
  #ifdef wifi_ssid4
  wifiMulti.addAP(wifi_ssid4, wifi_password4);
  #endif
  #ifdef wifi_ssid5
  wifiMulti.addAP(wifi_ssid5, wifi_password5);
  #endif
//  WiFi.begin(wifi_ssid1, wifi_password1);
//  for (int i=1; i<=10; i++) {
//    if (WiFi.status() == WL_CONNECTED) { break; };
//    // exit if successfully connected
//    delay(500);
//    Serial.print(".");
//  }
//  Serial.print("   IP address: ");
//  Serial.println(WiFi.localIP());

  Serial.println("Connecting Wifi...");
  if (wifiMulti.run() == WL_CONNECTED) {
    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
  }


  Serial.print("Configuring OTA device...");
  TelnetServer.begin();   //Necesary to make Arduino Software autodetect OTA device

  ArduinoOTA.onStart([]() {
    Serial.println("OTA starting...");
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("OTA update finished!");
    Serial.println("Rebooting...");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("OTA in progress: %u%%\r\n", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });
  ArduinoOTA.begin();
  Serial.println("Wifi OK");
}


void setup_mcp9808() {
    Serial.println("setup MCP9808");
  
  // Make sure the sensor is found, you can also pass in a different i2c
  // address with tempsensor.begin(0x19) for example
  //  Wire.pins(2, 14); 

  if (!mcp9808.begin(0x18)) {
    Serial.println("Couldn't find MCP9808!");
    while (1);
  }

}

void setup() {
  Serial.begin(112500);

  Serial.println(F("Setup BME280"));

  bool status;

  i2c.begin(2, 14);
  
  //setup_mcp9808();
  
  status = bme.begin(0x76,&i2c);
  if (!status) {
    Serial.println("Could not find a valid BME280 sensor, check wiring!");
    //while (1);
  } else {
    Serial.println("found valid BME280 sensor!");
  }

    Serial.println("-- Weather Station Scenario --");
    Serial.println("forced mode, 1x temperature / 1x humidity / 1x pressure oversampling,");
    Serial.println("filter off");
    bme.setSampling(Adafruit_BME280::MODE_FORCED,
                    Adafruit_BME280::SAMPLING_X1, // temperature
                    Adafruit_BME280::SAMPLING_X1, // pressure
                    Adafruit_BME280::SAMPLING_X1, // humidity
                    Adafruit_BME280::FILTER_OFF   );
                      
    // suggested rate is 1/60Hz (1m)
   // delayTime = 60000; // i&i2cn milliseconds

  
  setup_wifi();
  u8g2.begin();
  u8g2.setFlipMode(1);
}

int x = 0;
void loop() {
  bme.takeForcedMeasurement(); // has no effect in normal mode
  printValues_bme280();
  //printValues_mcp9808();
  if (wifiMulti.run() != WL_CONNECTED) {
    Serial.println("WiFi not connected!");
   
  }

  ArduinoOTA.handle();
  u8g2.clearBuffer();          // clear the internal memory
  u8g2.setFont(u8g2_font_6x10_mf); // choose a suitable font
  u8g2.drawStr(0, 8, text); // write something to the internal memory
  IPAddress myip = WiFi.localIP();
  String sFullip = String(myip[0]) + "." + myip[1] + "." + myip[2] + "." + myip[3];
  u8g2.drawStr(0,19 , sFullip.c_str()); // write something to the internal memory
  u8g2.setCursor(0,32);
  
  String sensorstats = String(bme.readTemperature()) + "C " 
                     + String(bme.readPressure()/100.0F) + "hPa "
                     + String(bme.readHumidity()) + "%";
  u8g2.print(sensorstats);
  u8g2.sendBuffer();          // transfer internal memory to the display
  x++;
  delay(6000);
  
}


void printValues_mcp9808() {
  Serial.print("mcp9808 Temperature = ");
  Serial.print(mcp9808.readTempC());
  Serial.println(" *C");
  Serial.println();
}


void printValues_bme280() {
  Serial.print("bme280 Temperature = ");
  Serial.print(bme.readTemperature());
  Serial.println(" *C");

  Serial.print("Pressure = ");

  Serial.print(bme.readPressure() / 100.0F);
  Serial.println(" hPa");

  Serial.print("Approx. Altitude = ");
  Serial.print(bme.readAltitude(SEALEVELPRESSURE_HPA));
  Serial.println(" m");

  Serial.print("Humidity = ");
  Serial.print(bme.readHumidity());
  Serial.println(" %");

  Serial.println();
}
